const webRoutes = require('./src/web/app');
const Products = require('./src/uber/Products');
const ProductEstimates = require('./src/uber/ProductEstimate');
const Book = require('./src/uber/Book');
const FavPLaces = require('./src/uber/FavPlaces');
const Profile = require('./src/menu/Profile');
const Add_coupon = require('./src/menu/AddCoupon');
const CancelAnyThread = require('./src/menu/Cancel');
const GetHistory = require('./src/menu/GetHistory');
const Menu = require('./src/menu/Menu');
const CurrentTrip = require('./src/menu/ViewCurrentTrip');
const CancelTrip = require('./src/uber/CancelTrip');
const AfterAuth = require('./src/uber/AfterAuth');
const axios = require('axios');


module.exports = function(bp) {
  webRoutes(bp);
  ProductEstimates(bp);
  Products(bp);
  Book(bp);
  FavPLaces(bp);
  Profile(bp);
  Add_coupon(bp);
  CancelAnyThread(bp);
  CancelTrip(bp);
  GetHistory(bp);
  Menu(bp);
  CurrentTrip(bp);
  CancelTrip(bp);
  AfterAuth(bp);

  bp.hear(['Hi', 'hi', 'GET_STARTED', 'hello', 'Hello'], (event, next) => {
    event.reply('#greeting')
    .then(() => bp.messenger.sendText(event.user.id, "If you are not automatically messaged after you have been authenticated, please use Menu and select option 'Menu' to manually start booking.")) // See the file `content.yml` to see the block

    //save the Ola token
  
    bp.db.kvs.set(`USERNAME/${event.user.id}/userid`, {user_id: event.user.id})
      .then(() => console.log("User ID saved in kvs"))
      .catch(() => console.log('User ID could not be saved in DB'))
  });

  // bp.hear(/GET_STARTED|hello|hi|test|hey|holla/i, (event, next) => {
  //   event.reply('#welcome') // See the file `content.yml` to see the block
  // });

  bp.hear({
    type: /message|text/i,
    text: /exit|bye|goodbye|quit|done|leave|stop/i
  }, (event, next) => {
    event.reply('#goodbye', {
      // You can pass data to the UMM bloc!
      reason: 'unknown'
    });
  });
};
