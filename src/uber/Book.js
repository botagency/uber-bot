const _ = require('lodash');
const axios = require('axios');
const uber = require('../config/Uber');
const uber_webhook = require('./webhook/Uber');
const txt = (bp, id, txt) => bp.messenger.sendText(id, txt)

module.exports = bp => {

    bp.hear({
        type: 'postback',
        text: /CCRV$/
    }, (event, next) => {
        let token = uber.access_token

        let conv = bp.convo.find(event);
        if (conv){
            bp.logger.warn('User already has an unresolved event');
            conv.stop('aborted');
            return bp.messenger.sendText(event.user.id, "You had an unresolved convo. I stopped it for you, but you have to start again. Use the menu to cancel your trip instead");
        }

        //uber webhook here
        uber_webhook(bp, event);
        if (!token) {
           return bp.messenger.sendText(event.user.id, "You need to be reauthorized")
            .then(()=> event.reply('#refreshButton'))
        } else if (token) {
        let fare_id = event.text.replace('CCRV', '').trim();
        console.log(`Fare ID is:: ${fare_id}`);
        bp.convo.start(event, convo => {
            bp.db.kvs.get(`USER/${event.user.id}/current_loc`)
            .then(current => {
                console.log('Saved current location is::::')
                console.log(current)
                convo.set('start_lat', current.location.lat);
                convo.set('start_long', current.location.long);
            })
            .catch(err => console.log('Error getting saved location from KVS'));

            bp.db.kvs.get(`USER/${event.user.id}/end_loc`)
            .then(end => {
                convo.set('end_lat', end.location.lat);
                convo.set('end_long', end.location.long);
            })
            .catch(err => console.log('Error getting end location from KVS'));

convo.on('done', ()=> {
    let start_lat = convo.get('start_lat');
    let start_long = convo.get('start_long');
    let end_lat = convo.get('end_lat');
    let end_long = convo.get('end_long');
    console.log(`Start lat: ${start_lat}, Start_long: ${start_long}`);
    console.log(`End lat: ${end_lat}, End_long: ${end_long}`);

    uber.requests.getCurrentAsync()
    .then(response => {
        if (response) {
            bp.messenger.sendText(event.user.id, "You are already on a trip. Cancel that first");
            return event.reply('#cancelTripButton');
        } 
    })
    .catch(error => {
        bp.logger.debug('========>>>>>DEBUG<<<<<==========');
        bp.logger.info('Error getting current trip')
        console.log(error.body.errors[0].status);

        if (error.body.errors[0].status === 404) {


            bp.db.kvs.get(`USER/${event.user.id}/current_loc`)
            .then(loc => {
                let start_lat = loc.location.lat;
                let start_long = loc.location.long;

                bp.db.kvs.get(`USER/${event.user.id}/end_loc`)
                .then(res => {
                    let stop_lat = res.location.lat;
                    let stop_long = res.location.long;


                    //replace with the user dynamically gotten location
                    //the majority of the code below is to simulate the flow
                    //of the webhook event in production. Removed when switching from
                    //sandbox to production.
                    uber.requests.createAsync({
                        "fare_id": fare_id,
                        "start_latitude": 37.341120,
                        "start_longitude": -121.899834,
                        "end_latitude": 37.345515,
                        "end_longitude": -121.906614
                    })
                    .then(response => {
                        console.log('Requests result');
                        console.log(response);
        
                        txt(bp, event.user.id, "Your request is processing. Please hangon. . .")
                        if (response.status === 'processing') {
        
                            uber.requests.setStatusByIDAsync(response.request_id, 'accepted')
                            .then(res => {
                                bp.logger.debug('User trip changed to accepted');
                                console.log(res);
        
                                setTimeout(() => {
                                    uber.requests.setStatusByIDAsync(response.request_id, 'arriving')
                                    .then(res => bp.logger.debug(`Status changed to arriving`))
                                    .catch(err => bp.logger.debug(`Error setting to arriving`))
                                }, 20000);
        
                                setTimeout(() => {
                                    uber.requests.setStatusByIDAsync(response.request_id, 'in_progress')
                                    .then(res => bp.logger.debug(`Status changed to in progress`))
                                    .catch(err => bp.logger.debug(`Error setting to in progress`))
                                }, 35000);
        
        
                                setTimeout(() => {
                                    uber.requests.setStatusByIDAsync(response.request_id, 'completed')
                                    .then(res => {bp.logger.debug(`Status changed to completed`); 
                                    uber.requests.getByIDAsync(response.request_id)
                                    .then(reply => {
                                        bp.logger.debug(`User details gotten via request ID`);
                                        bp.logger.info(reply);
                                    })
                                    .catch(err => {
                                        bp.logger.debug(`Error getting details with requesst id`)
                                        bp.logger.error(err);
                                    });
                                })
                                    .catch(err => bp.logger.debug(`Error setting to completed`))
                                }, 60000);
        
                            })
                            .catch(err => {
                                bp.logger.error('Error updating trip');
                                console.log(err.message);
                                console.log(err);
                                txt(bp, event.user.id, "Uh-Oh!! There was an error processing your request. Please use the menu to see if you are on any trip.")
                            });
                            
                        }
                    })
                    .catch(error => {
                        console.log(error.response);
        
                        switch(error.response.body.errors[0].status){
        
                            case 404:
                                txt(bp,event.user.id, error.response.body.errors[0].title)
                                .then(() => event.reply('#actionButtons'));
                            break;
        
                            case 400:
                                txt(bp,event.user.id, error.response.body.errors[0].title)
                                .then(() => event.reply('#actionButtons'));
                            break;
        
                            case 403:
                                txt(bp,event.user.id, error.response.body.errors[0].title)
                                .then(() => event.reply('#actionButtons'));
                            break;
        
                            case 409:
                                txt(bp,event.user.id, error.response.body.errors[0].title)
                                .then(() => event.reply('#actionButtons'));
                            break;
        
                            case 422:
                                txt(bp,event.user.id, error.response.body.errors[0].title)
                                .then(() => event.reply('#actionButtons'));
                            break;
        
                            case 500:
                                txt(bp, event.user.id, error.response.body.errors[0].title)
                                .then(() => event.reply('#actionButtons'));
                            break;
                            
                        }
                    // if (error.response.body.errors[0].code === 'no_drivers_available) {
                    //     bp.logger.debug(`No available drivers for this trip!!`);
                    // }
                        console.log('An error occured when making calls to request endpoint');
                        console.log(error.response.body);
                    });


                })
                .catch(err => bp.logger.error(err))
            })
            .catch(err => bp.logger.error(err))

}
    })
  })
})
  } 
})
}