const uber = require('../config/Uber');

module.exports = bp => {
  const debug = txt => bp.logger.debug(`DEBUG ======> ${txt}`);
  const txt = (txt,id) => bp.messenger.sendText(id, txt);
  const error = txt => bp.logger.error(`ERROR ======> ${txt}`);
  const info = txt => bp.logger.info(txt);

  bp.hear({
    type: 'postback',
    text: 'places_view_fav'
  }, (event, next) => {
   
    debug("User wants to get his/her favourite places");

    event.reply('#favPlaces');
  });



  bp.hear('FAVPLACES.P1', (event, next) => {

    let token = uber.access_token;
    if (!token) {
        return bp.messenger.sendText(event.user.id, "You need to be reauthorized", {typing: true})
        .then(()=> event.reply('#refreshButton'));
    } else if (token) {
      debug("User wants to see home");
      uber.places.getHomeAsync()
      .then(res => {
        debug('User has gotten home details');

        txt(`Here is the address to you home:
${res.address}`).then(() => event.reply('endAction'));
        info(res);
      })
      .error(err => {
        debug("User got error from asking for home details");
        info(err.body.code);
        info(err.body)

        switch(err.body.code) {
          case 'service_unavailable':
            txt(`The service is currently unavailable. Please try again`, event.user.id)
            .then(() => event.reply('#actionButtons'));
          break;

          case 'unauthorized':
            txt(`You are not authorized to perform that action. Please try and re-authorize yourself`, event.user.id)
            .then(() => event.reply('#refreshButton'));
          break;

          case 'validation_failed':
            txt(`Validation failed! Please try again!`)
            .then(() => event.reply('#actionBUttons'));
          break;

          case 'not_implemented':
            txt(`You are yet to add that to your account. Please try again after you do so`)
            .then(()=> event.reply('#actionButton'));
          break;
        }
      })
    }
  })



  bp.hear('FAVPLACES.P2', (event, next) => {

    let token = uber.access_token;
    if (!token) {
        return bp.messenger.sendText(event.user.id, "You need to be reauthorized", {typing: true})
        .then(()=> event.reply('#refreshButton'));
    } else if (token) {
      debug("User wants to see Work");
      uber.places.getWorkAsync()
      .then(response => {
        debug('User has gotten work details');

        info(response);
      })
      .error(err => {
        debug("User got error from asking for work details");

        txt(`Here is the address to your place of work:
${res.address}`).then(() => event.reply('endAction'));
        info(err.body.code);

        switch(err.body.code) {
          case 'service_unavailable':
            txt(`The service is currently unavailable. Please try again`, event.user.id, {typing: true})
            .then(() => event.reply('#actionButtons'));
          break;

          case 'unauthorized':
            txt(`You are not authorized to perform that action. Please try and re-authorize yourself`, event.user.id, {typing: true})
            .then(() => event.reply('#refreshButton'));
          break;

          case 'validation_failed':
            txt(`Validation failed! Please try again!`)
            .then(() => event.reply('#actionBUttons'));
          break;

          case 'not_implemented':
            txt(`You are yet to add that to your account. Please try again after you do so`)
            .then(()=> event.reply('#actionButton'));
          break;
        }

        // info(err.response.body.errors[0].status);

        // switch(err.response.body.errors[0].status) {
        //   case 401:
        //   txt(`You are not authorized to perform that action. Please try and re-authorize yourself`, event.user.id)
        //   .then(() => event.reply('#refreshButton'));
        //   break;

        //   case 404:

        // }

      });
    }
  });
}