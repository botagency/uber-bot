const lodash = require('lodash');
const axios = require('axios')
let start_coordinates = [];
let end_coordinates = [];
const uber = require('../config/Uber');
const txt = (bp,id, txt) => bp.messenger.sendText(id, txt)
module.exports = bp => {
    bp.hear({
        type: 'postback',
        text: /XXCVR$/
    }, (event, next) => {
        let text = event.text.replace('XXCVR', ' ');
        console.log('the original text is::', event.text);
        let id = text.trim();

        let token = uber.access_token;

        
        if (!token || token === undefined || token === null) {
            const conv = bp.convo.find(event)
            if (conv) {
                console.log('Previous convo stopped');
                conv.stop();
                return bp.messenger.sendText(event.user.id, "Okay, convo stopped")
                .then(()=> event.reply('#refreshButton'));
            }
        } else if (token) {
        bp.convo.start(event, convo => {

            bp.db.kvs.get(`USER/${event.user.id}/current_loc`)
            .then(loc => {
                let start_lat = loc.location.lat;
                let start_long = loc.location.long;

                convo.set('start_lat', start_lat);
                convo.set('start_long', start_long);
            });

            bp.db.kvs.get(`USER/${event.user.id}/end_loc`)
            .then(res => {
                let stop_lat = res.location.lat;
                let stop_long = res.location.long;

                convo.set('end_lat', res.location.lat);
                convo.set('end_long', res.location.long);

                convo.next(); 
            });

            convo.on('done', () => {
                let start_lat = convo.get('start_lat');
                let start_long = convo.get('start_long');
                let end_lat = convo.get('end_lat');
                let end_long = convo.get('end_long');

                console.log(`Start lat: ${start_lat}, Start long: ${start_long}, Stop lat: ${end_lat}, Stop long: ${end_long}`);

                // if (!uber.access_token) {
                //     console.log('User token is invalid. To handle this, ask the user to re-authorize');
                // }

                if (uber.isTokenStale === true) {
                    console.log('User token is stale. Refresh it');
                }

                
                    uber.requests.getEstimatesAsync({
                        "products_id": id,
                        "start_latitude": 37.341120,
                        "start_longitude": -121.899834,
                        "end_latitude": 37.345515,
                        "end_longitude": -121.906614
                      })
                      .then(res => {
                          console.log(res);
                          
                          const arr = {
                            distance_estimate: res.trip.distance_estimate,
                            distance_unit: res.trip.distance_unit,
                            pickup_estimate: res.pickup_estimate,
                            estimate_display: res.fare.display,
                            fare_id: res.fare.fare_id
                          }
                          event.reply('#EstimateButton', {
                              distance_estimate: arr.distance_estimate,
                              distance_unit: arr.distance_unit,
                              pickup_estimate: arr.pickup_estimate,
                              estimate_display: arr.estimate_display,
                              fare_id: arr.fare_id
                          });
                        })
                      .catch(error => console.log(error.response.data.errors[0].status));
            });
        });
    }
    });
};