const uber = require('../config/Uber');
const _ = require('lodash');

module.exports = bp => {
    bp.hear(['cancelTripButton.C1', 'CANCELTRIPBUTTON.C1'], (event, next) => {
        bp.logger.debug(`Debug ==> User wants to clear current trip from quick reply button`);

        let token = uber.access_token;

    let conv = bp.convo.find(event);
        if (conv){
            bp.logger.warn('User already has an unresolved event');
            conv.stop('aborted');
            return bp.messenger.sendText(event.user.id, "You had an unresolved convo. I stopped it for you, but you have to start again. Use the menu to cancel your trip instead");
        }

    if (!token) {
        return bp.messenger.sendText(event.user.id, "You need to be reauthorized")
        .then(()=> event.reply('#refreshButton'))
    } else if (token) {
        bp.convo.start(event, convo => {
            convo.messageTypes = ['quick_reply'];
            convo.threads['default'].addQuestion('#cancelConfirm', response => {
                bp.logger.debug(`Debug ==> Cancel from QR convo thread response from QR`);
                console.log(response.text);
                convo.set('response', response.text);
                convo.next();
            });

            convo.on('done', ()=>{
                let response = convo.get('response');

          if (response === 'CANCELCONFIRM.YES') {

            uber.requests.getCurrentAsync()
            .then(res => {
                bp.logger.debug('User current trip gotten');
    
                uber.requests.deleteCurrentAsync()
                .then(res => {
                    bp.logger.debug('User deleted their current trip');
                    bp.messenger.sendText(event.user.id, "Okay! Your trip has been cancelled", {typing: true})
                    .then(()=> event.reply('#endAction'));
                })
                .catch(error => {
                    bp.logger.debug('Error deleting user current trip');
                    console.log(error);
                    bp.messenger.sendText(event.user.id, "Urh-Oh! I am having issues deleting your trip. Dont worry, the blame is on me, but please try again")
                });
            })
            .catch(err => {
                if (err.body.errors[0].status === 404) {
                    bp.messenger.sendText(event.user.id, 'You dont seem to be on any trip. Why not start one?')
                .then(() => event.reply('#actionButtons'));
                }
                bp.logger.error('Error getting user current trip');
            });

                } else if (response === 'CANCELCONFIRM.NO') {
                    
                    return bp.messenger.sendText(event.user.id, 'Alright, I\'ve got your back, jack :)')
                    .then(()=>  event.reply('#actionButtons'));
                }
                bp.logger.debug('QR cancel thread finished');
            });
        });
    }
    
    });
};
