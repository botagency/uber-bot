/**esversion:6 */
const uber = require('../config/Uber');
module.exports = bp => {
    bp.hear(['ACTIONBUTTONS.B1', 'actionButtons.B1'], (event, next) => {
        console.log('User selected the products option');

    const txt = txt => bp.messenger.sendText(event.user.id, txt);

    let conv = bp.convo.find(event);
        if (conv){
            bp.logger.warn('User already has an unresolved event');
            conv.stop();
            return bp.messenger.sendText(event.user.id, "You had an unresolved convo. I stopped it for you, but you have to start again.");
        }
            
    bp.convo.start(event, convo =>{
        convo.messageTypes = ['postback', 'quick_reply', 'location'];
        convo.threads['default'].addQuestion('#locationButton', response => {
            bp.logger.info('Shit, it worked!!');
            convo.set('current_lat', response.raw.payload.coordinates.lat);
            convo.set('current_long', response.raw.payload.coordinates.long);
            bp.db.kvs.set(`USER/${event.user.id}/current_loc`, {location: response.raw.payload.coordinates})
            .then(res => console.log('User location saved in the KVS'))
            .catch(err => console.log('Error saving location in KVS'));
            convo.switchTo('secondLocation')
        });

        convo.createThread('secondLocation');
        convo.threads['secondLocation'].addQuestion('#stopLocationButton', response => {
            convo.set('end_lat', response.raw.payload.coordinates.lat);
            convo.set('end_long', response.raw.payload.coordinates.long);
            bp.db.kvs.set(`USER/${event.user.id}/end_loc`, {location: response.raw.payload.coordinates})
            

            bp.db.kvs.get(`USER/${event.user.id}/current_loc`)
            .then(res => {
                console.log('User location grabbed is:::')
                console.log(`LATITUDE ===> ${res.location.lat}`);
                console.log(`LONGITUTDE ===> ${res.location.long}`)
                let str_lat = res.location.lat;
                let str_long = res.location.long;

                convo.set('start_lat', str_lat);
                convo.set('start_long', str_long);
                
                convo.next();
            });
        });
        
        convo.on('done', ()=>{
            let myArr = [];
            let lat = convo.get('current_lat');
            let long = convo.get('current_long');
            let end_lat = convo.get('end_lat');
            let end_long = convo.get('end_long');
            console.log(`Start lat: ${lat} Start long: ${long} End lat:${end_lat} End Long:${end_long}`);

            uber.estimates.getPriceForRouteAsync(37.341120, -121.899834, 37.345515, -121.906614)
            .then(response => {
                bp.logger.info(`Estimate for the location is::`);
                bp.logger.info(response);

                if (response.prices.length === 0) {
                    return txt(`Urh-Oh! I couldn't find the appropriate estimates.`, {typing: true})
                    .then(() => txt(`That could mean Uber isn't available at your location. Please try another location.`, {typing:2000}))
                }
                let arr = [];
                //loop through the price object.
                for (let i = 0; i < response.prices.length; i++) {
                    let base  = response.prices[i];
                    let dur = (base.duration)/60;
                    arr.push({
                        title: base.display_name,
                        id: base.product_id,
                        duration: dur,
                        distance: base.distance,
                        estimate: base.estimate
                    });
                }
                event.reply('#productsCarousel', {myArr: arr});
            })
            .catch(err => {
                bp.logger.info(`Error getting price estimate`);
                bp.logger.info(err.message);
            });
        });
        });
    });
};