const rt = require('../../config/Router');
const uber = require('../../config/Uber');
module.exports = (bp, event) => {
    const router = rt(bp);

    const debug = txt => bp.logger.debug(`DEBUG ======> ${txt}`);
    const txt = txt => bp.messenger.sendText(event.user.id, txt);
    const error = txt => bp.logger.error(`ERROR ======> ${txt}`);
    const info = txt => bp.logger.info(txt);

    router.post('/uber/webhook', (req, res) => {
        let id = event.user.id;
        let body = req.body;
        //let num = 09030829137;
        if (body.meta.status === 'accepted') {
            bp.messenger.sendText(id, "Your request has been accepted. A driver is on the way. Below are the details and the details of your trip")
            .then(()=> {
                uber.requests.getCurrentAsync()
                .then(response => {
                    debug("User trip");
                    info(response);
                    let request_id = response.request_id;

                    event.reply('#tripDetails', {
                        eta: response.pickup.eta,
                        image: response.vehicle.picture_url,
                        make: response.vehicle.make,
                        model: response.vehicle.model,
                        license: response.vehicle.license_plate,
                        number: response.driver.phone_number,
                        
                        
                        country: response.pickup.country_name,
                        
                        driver_url: response.driver.picture_url,
                        
                        driver_name:  response.driver.name,
                        rating: response.driver.rating,
                        sms_no: response.driver.sms_number,
                        

                    })
                    .then(()=> {
                        event.reply('#tripInfo', {
                            address: response.pickup.address,
                            pickup_label: response.pickup.alias,
                            pickup: response.pickup.region.name,
                            code: response.pickup.country_code,
                            destination_address: response.destination.address,
                            destination_eta: response.destination.eta,
                            destination_label: response.destination.alias,
                        });
                    })
                    bp.db.kvs.set(`USER/${id}/request_id`, {id: request_id})
                    .then(()=> debug("REQUEST ID SAVED TO KVS"))
                    .catch(()=> error("ERROR SAVING REQUEST_ID TO KVS"));
                })
                .catch(err => {
                    if (err) {
                        txt("You are not currently on any trip. Why not start one?")
                        .then(()=> event.reply('#endAction'));
                    }
                    error("Error in getting trip");
                    info(err);
                })
            });
        //     .then(() => {
        // uber.requests.getCurrentAsync()
        // .then(res => {
        //     bp.logger.debug('User current trip gotten');
        //     bp.db.kvs.set(`USER/${id}/request_id`, {id: res.request_id});
        //     txt('Here are the details of your current trip')
        //     .then(()=> event.reply('#tripDetails', {
        //         title: res.driver.name,
        //         rating: res.driver.rating,
        //         image: res.vehicle.picture_url,
        //         make: res.vehicle.make,
        //         model: res.vehicle.model,
        //         license: res.vehicle.license_plate,
        //         number: res.driver.phone_number
        //     }))
        //      .then(()=> event.reply('#endAction'));
        // })
        // .catch(error => {
        //     error(error)
        //     if (error) {
        //         bp.messenger.sendText(event.user.id, 'You dont seem to be on any trip. Why not start one?')
        //     .then(() => event.reply('#actionButtons'));
        //     }
        //    error('Error getting user current trip');
        // });
        //     });
        } 
        
        
        else if (body.meta.status === 'rider_canceled') {
           debug("You cancelled a rider without this button listening")
        }


        else if(body.meta.status === 'ready') {
            debug("User receipt is ready");
            bp.db.kvs.get(`USER/${id}/request_id`)
            .then(response => {
                debug("USER REQ. ID FROM KVS");
                info(response);
                let request_id = response.id;
                uber.requests.getReceiptByIDAsync(request_id)
                .then(rec => {
                    debug("User receipt gotten");
                    info(rec);

                    txt(`The receipt for your trip is:
Distance:${rec.distance} ${rec.distance_label}
Title:  ${rec.charge_adjustments[0].name}
Total Owed:  ${rec.total_owed}
Duration:  ${rec.duration} 
Subtotal:  ${rec.subtotal}
Total fare:  ${rec.total_fare}`);

                    // event.reply('#receipt', {
                    //     charge_name: rec.charge_adjustment[0].name,
                    //     total_owed: rec.total_owed,
                    //     duration: rec.duration,
                    //     fare: rec.total_fare,
                    //     dist_label: rec.distance_label,
                    //     subtotal: rec.subtotal
                    // })
                })
                .catch(err => {
                    error("Error getting user receipt");
                    info(err)
                });
            })
            .catch(err => {
            error("Error getting user request_id from KVS");
            console.log(err);
        });
        } else if (body.meta.status === 'no_drivers_available') {
            debug(`Webhook Event. . . . . . No drivers available`);

            bp.messenger.sendText(event.user.id, 'Oops!! There are no drivers available! Please try again.')
        } else if (body.meta.status === 'arriving') {
            txt(`Your ride has arrived! Please be nice to your co-riders and/or driver.`);
        } else if (body.meta.status === 'in_progress') {
            txt(`Trip is currently in progress. I hope you are enjoying your trip. :)`);
        } else if (body.meta.status === 'driver_canceled') {
            txt(`Oops!! Your driver canceled the trip! Don't worry, you can always book another driver`);
            bp.logger.debug(`=====>>>DEBUG<<<======`);
            bp.logger.info(body);
        } else if (body.meta.status === 'completed') {

            debug(`TRIP COMPLETED. . . . `);
            txt(`Awesome!! The trip has been successfully completed. Your receipt in a bit. . . `)
            .then(() => txt(`I hope you had a good trip. Please don't hesitate to book next time`))
            .then(() => event.reply('#endAction'))
            .then(() => event.reply('#actionButtons'));
        }

        //else if (bo)
        console.log('======================');
        console.log('=================');
        console.log('============');
        info(body);
        console.log('============');
        console.log('=================');
        console.log('=====================');
    
        res.status(200).send('Ping!!');
      });
    

      

}