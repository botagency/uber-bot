const axios = require('axios');
const uber = require('../config/Uber');
const path = require('path')
const event = require('events');
const route = require('../config/Router');
let myToken = null;
let user_id = null
let authURL = uber.getAuthorizeUrl(['history', 'profile', 'request', 'places', 'request_receipt', 'ride_widgets'])
module.exports = bp => {

  const router = route(bp)
  const debug = txt => bp.logger.debug(txt);
  //const router = bp.getRouter('botpress-ui');
  
  router.get('/auth', (req, res) => {
    bp.logger.info('Authorizing User. . . ');

    res.redirect(authURL)
  });

  router.get('/ola/redir', (req, res) => {
    
    res.sendFile(path.join(__dirname, '../../static/authed-ola.html'))
  })

  router.get('/reauth', (req, res) => {
    bp.logger.info('Re-authing user');

    res.redirect(authURL);
  });

  router.post('/authed', (req, res) => {
    let id = req.body.psid;
    const text = 'Now that you are authorized, please click to proceed.'
    const options = {
      quick_replies: [{
        content_type: "text",
        title: "Okay Got it",
        payload: 'xxcrw'
      }]
    }
    console.log('user id is:::', id);
    console.log('======================');
    console.log('=========================')
    

    
    // console.log(myId);
    // bp.db.kvs.set(`USERS/TOKEN/${id}/token`, {token: myToken})
    // .then(() => console.log('User token has been saved'))
    // .catch(() => console.log('Error saving token to KVS'));
    bp.messenger.sendText(id, text, options, {typing: true})
      .then(() => console.log('User location gotten, after verification'))


      

    res.status(200).send('Ping!!')
  });


  router.post('/ola/redir', (req, res) => {
    debug(`POSTing successfull. The Ola-token is: ${req.body.ola_token}`);
    debug(`The user ID is: ${req.body.psid}`);

    bp.db.kvs.set(`USER/${req.body.psid}/ola_token`, {token: req.body.ola_token})
    .then(() => debug(`Saved Ola token to KVS`))
    .catch(() => debug(`Error saving to KVS`));
    
    res.status(200).send('Ping from ola postings');
  });

  router.get('/test', (req, res) => {

    uber.requests.createAsync({
      "fare_id": "dglkodgdgodfpgklp[g",
      "start_longitude": -121.899834,
      "end_latitude": 37.345515,
      "end_longitude": -121.906614,
  })
  .then(response => {
    bp.logger.debug("Debug  ========>>>>>");
    bp.logger.info(response);
  })
  .error(err => {
      //"start_latitude": 37.341120,
    bp.logger.debug("Debug ========>>>>>");
    bp.logger.info(err);
  })

  res.status(200).send('OK');

  });

  router.get('/webhook/aval', (req, res) => {

    //bp.db.kvs.get(`USER`)
    axios({
      method: 'put',
      url: 'https://sandbox-api.uber.com/v1.2/sandbox/products/57c0ff4e-1493-4ef9-a4df-6b961525cf92',
      header:{
        'Authorization': `Bearer `
      },
      data: {
        'surge_multiplier': '2.2',
        'drivers_available' : 'false'
      }
    })
    .then(response => {
      bp.logger.info('successfully set the surge and avail.');
      bp.logger.info(response)
    })
    .catch(err => {
      bp.logger.debug('Error setting stuff');
      bp.logger.error(err.response.data.message);
    })

    res.status(200).send('It works!!!');
  });


  router.get('/webhook/redir', (req, res) => {
    uber.authorizationAsync({
      authorization_code: req.query.code
    })
      .spread((access_token, refresh_token, authed_scopes, token_exp) => {
        console.log('User access token');
        bp.logger.debug(access_token);

        user_id = req.body.psid
        console.log('======================');
        console.log('=========================');
        
        console.log('Refresh_token');
        bp.logger.info(refresh_token);

        console.log('Token Expiration');
        bp.logger.info(token_exp);
        bp.logger.info(`OOOOKKKKKAAAAAYYYYYY. USER ID GOTTEN. SENDING MESSAGE TO USER IN THE CASE OF A ERROR`);
        bp.logger.info(user_id);
      })
      .error(err => {
        console.log('Error spreading');
        bp.logger.info(err);
        bp.messenger.sendText(user_id, 'Oops!!There was an error authenticating you. Please try again. If it persists, try logging out of your logged in Uber account on the browser (not app).');
      });

    res.sendFile(path.join(__dirname, '../../static/authed.html'))
  });

  
  router.get('/ola/webhook', (req, res) => {
    
    res.status(200).send('Pong from ola-get');
  });

};