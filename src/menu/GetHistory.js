const _ = require('lodash');
const moment = require('moment');
const uber = require('../config/Uber');
module.exports = bp => {
    bp.hear({
        type: 'postback',
        text: 'history_trip_get'
    }, (event, next) => {
        let token = uber.access_token;

        bp.logger.debug(`=======>DEBUG<======= User wants to get history.`)
        if (!token) {
            return bp.messenger.sendText(event.user.id, "You need to be reauthorized")
            .then(()=> event.reply('#refreshButton'))
        } else if (token) {
        const conv = bp.convo.find(event);
        if (conv){
            bp.logger.warn('User already has an unresolved event');
            conv.stop('Aborted the convo');
            return bp.messenger.sendText(event.user.id, "You had an unresolved convo. I stopped it for you, but you have to start again.");
        }

bp.convo.start(event, convo => {
    convo.threads['default'].addQuestion('#historyLimit', [
        {
            pattern: /(\d+)/i,
            callback: (response) => {
                if (response.match > 10) {
                    bp.messenger.sendText(event.user.id, "Sorry, cant be more than 10");
                    convo.repeat()
                } else {
                    let limit = response.match;
                    bp.logger.info(`User wants past ${limit} history`)
                    convo.set('limit', limit);
                    convo.next();
                }
            }
}, {
            default: true,
            callback: () => {
                bp.messenger.sendText(event.user.id, "That didnt look like a number. Please try again");
                convo.repeat();
            }
        }
    ]);

    convo.on('done', () => {
        let limit = parseInt(convo.get('limit'), 10);

        bp.logger.debug('User History enquiry event done');
        uber.user.getHistoryAsync(0, limit)
        .then(res => {
            bp.logger.debug('User history retrieved');
            console.log(res);
            console.log(res.history);
            if (res.history.length === 0) {
                bp.messenger.sendText(event.user.id, 'Urh-Oh! Seems you haven\'t had existing history. Why not start one?')
                .then(()=> event.reply('#actionButtons'));
            } else {
                bp.logger.debug('History JSON Array');
                console.log(res);
                const array = [];
                bp.messenger.sendText(event.user.id, `Here comes your last ${limit} histories`)
                
                for (let i =0; i < res.history.length; i++) {
                    let base  = res.history[i];
                    array.push({
                        title: base.start_city.display_name,
                        subtitle: base.status,
                        id : base.product_id,
                        request_id : base.request_id,
                        start_time: base.start_time,
                        stop_time: base.stop_time
                    })
                }
                let start_time = moment(array.start_time).calendar();
                let stop_time = moment(array.stop_time).calendar();
                event.reply('uberHistory', {
                    name: array.title,
                    status: array.subtitle,
                    id: array.id,
                    start_time: start_time,
                    stop_time: stop_time
                })
            }
        })
        .catch(error => {
            bp.logger.error('Error getting user history');
            console.log(error);
        })
    })
});
    }
    })
}