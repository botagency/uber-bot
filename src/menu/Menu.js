const _ = require('lodash');

module.exports = bp => {
    bp.hear({
        type: 'postback',
        text: 'menu_view'
    }, (event, next) => {
        event.reply('#actionButtons');
    })


    bp.hear('menu_view', (event, next) => {
        event.reply('#actionButtons');
    });

    bp.hear(['TRIPSUCCESSFULL.MENU', 'Let\'s book a cab', 'I need a cab', 'Book an Uber', 'book a cab', 'Book a cab'], (event, next) => {
        bp.logger.debug("===>>>>Debug<<<<<=====");
        bp.logger.info("User wants to see the menu")
        event.reply('#actionButtons');
    });
}