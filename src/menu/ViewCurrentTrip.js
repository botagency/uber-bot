const uber = require('../config/Uber');
const txt = (bp, id, txt) => bp.messenger.sendText(id, txt);

module.exports = bp => {
    bp.hear({
        type: 'postback',
        text: 'current_trip_view'
    }, (event, next) => {
        let token = uber.access_token;

        bp.logger.debug(`=====>DEBUG<====== User wants to view current trip`);

        if (!token) {
            return bp.messenger.sendText(event.user.id, "You need to be reauthorized")
            .then(()=> event.reply('#refreshButton'));
        } else if (token){
        uber.requests.getCurrentAsync()
        .then(res => {
            bp.logger.debug('User current trip gotten');
            console.log(res);

            txt(bp, event.user.id, 'Here are the details of your current trip, and your driver\'s details', {typing: true})
            .then(()=> event.reply('#tripDetails', {
                        eta: res.pickup.eta,
                        image: res.vehicle.picture_url,
                        make: res.vehicle.make,
                        model: res.vehicle.model,
                        license: res.vehicle.license_plate,
                        number: res.driver.phone_number,
                        pickup: res.pickup.region.name,
                        pickup_label: res.pickup.alias,
                        country: res.pickup.country_name,
                        code: res.pickup.country_code,
                        driver_url: res.driver.picture_url,
                        address: res.pickup.address,
                        driver_name:  res.driver.name,
                        rating: res.driver.rating,
                        sms_no: res.driver.sms_number,
                        destination_address: res.destination.address,
                        destination_eta: res.destination.eta,
                        destination_label: res.destination.alias,
            }))
            .then(()=> {
                event.reply('#tripInfo', {
                    address: res.pickup.address,
                    pickup_label: res.pickup.alias,
                    pickup: res.pickup.region.name,
                    code: res.pickup.country_code,
                    destination_address: res.destination.address,
                    destination_eta: res.destination.eta,
                    destination_label: res.destination.alias,
                });
            })
            .then(()=> event.reply('#endAction'));

            if (res.status === 'no_drivers_available') {
                txt(bp, event.user.id, "There are no drivers found. Why not try and book again?")
                .event.reply('#actionButtons');
            }
        })
        .catch(err => {
            if (err.body.errors[0].status === 404) {
                bp.messenger.sendText(event.user.id, 'You dont seem to be on any trip. Why not start one?')
            .then(() => event.reply('#actionButtons'));
            } else if (err.body.errors[0].status === 403) {
                txt(bp, event.user.id, "You are forbidden from making this request! Perhaps you should try again");
            }
            bp.logger.error('Error getting user current trip');
        });
    }
    });
};