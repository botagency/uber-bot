const uber = require('../config/Uber');
const txt = (bp, id, txt) => bp.messenger.sendText(id, txt)
module.exports = bp => {
    bp.hear({
        type: 'postback',
        text: 'user_coupon_add'
    }, (event, next) => {
        bp.logger.debug(`User wants to add coupon`);
        const conv = bp.convo.find(event);
    if (conv) {
        convo.stop('Aborted');
        return bp.messenger.sendText(event.user.id, "I discovered a previous convo. I cleared it, but you sadly you have to start again because I am confused");
    }

    let token = uber.access_token

        if (!token) {
           return bp.messenger.sendText(event.user.id, "You need to be reauthorized")
            .then(()=> event.reply('#refreshButton'))
        } else if (token) 
        bp.convo.start(event, convo => {
            convo.threads['default'].addQuestion('#couponQuestion',[
                {
                    pattern:/(.*?)/,
                    callback: response => {
                        console.log(`Coupon:: ${response.text}`);
                            let coupon_code = response.text;
                            convo.set('coupon_code', coupon_code);
                            convo.next();
                        }
                }
            ]);
            

            convo.on('done', ()=> {
                let code = convo.get('coupon_code'); 
                uber.user.applyPromoAsync(code)
                .then(res => {
                    bp.logger.info('User coupon successfully added');
                    console.log(res.description);
                    console.log(res.promotion_code);
                    txt(bp, event.user.id, "Success!! Your coupon was added.")
                })
                .error(err => {
                    txt(bp, event.user.id, "Sorry, there was a problem adding the promo-code. Please check and try again.")
                    .then(() => event.reply('actionButtons'));
                    bp.logger.info('Error applying coupon code');
                    console.log(err);
                });
                bp.logger.debug('User coupon convo done')

            });
        });
    });
};