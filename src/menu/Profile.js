const _ = require('lodash');
const uber = require('../config/Uber');
module.exports = bp => {
    bp.hear({
        type: 'postback',
        text: 'check_user_profile'
    }, (event, next) => {

        const debug = txt => bp.logger.debug(txt);

        debug("User wants to get uber profile");
        let token = uber.access_token;
    if (!token) {
        return bp.messenger.sendText(event.user.id, "You need to be reauthorized")
        .then(()=> event.reply('#refreshButton'))
    } 
    else if (token) {
    uber.user.getProfileAsync()
    .then(res => {
        bp.logger.debug('User wants to get profile');
        console.log(res);
        
        event.reply('#profileCarousel', {
            first_name: res.first_name,
            last_name: res.last_name,
            image_url: res.picture,
            promo_code: res.promo_code,
            email: res.email,
            verified: res.mobile_verified
        })
        .then(()=> event.reply('#endAction'));
    })
    .catch(error => {
        console.log('Error getting the user profile')
    })
}
    });




    bp.hear('check_user_profile', (event, next) => {

        const debug = txt => bp.logger.debug(txt);

        debug("User wants to get uber profile");
        let token = uber.access_token;
    if (!token) {
        return bp.messenger.sendText(event.user.id, "You need to be reauthorized")
        .then(()=> event.reply('#refreshButton'))
    } 
    else if (token) {
    uber.user.getProfileAsync()
    .then(res => {
        bp.logger.debug('User wants to get profile');
        console.log(res);
        
        event.reply('#profileCarousel', {
            first_name: res.first_name,
            last_name: res.last_name,
            image_url: res.picture,
            promo_code: res.promo_code,
            email: res.email,
            verified: res.mobile_verified
        })
        .then(()=> event.reply('#endAction'));
    })
    .catch(error => {
        console.log('Error getting the user profile')
    })
}
    })
}