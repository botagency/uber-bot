module.exports = bp => {
  bp.hear(/cancel|cancle/ig, (event, next) => {
    if (bp.convo.find(event)) {
      bp.convo.find(event).stop(`Aborted the convo`);
      return bp.messenger.sendText(event.user.id, `Alright, ${event.user.first_name} I have cancelled that action. You know I am here to help.`)
      .then(() => event.reply('#actionButtons'));
    }
    else {
      bp.messenger.sendText(event.user.id, `There is no action to cancel, ${event.user.first_name}. Perhaps start one?`)
      .then(() => event.reply('#actionButtons'));
    }
  })
}